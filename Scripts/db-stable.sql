CREATE DATABASE [Finanzas]
go 
use [Finanzas]
go

if NOT EXISTS (select 1 from sys.tables where NAME = 'TiposMovimientos')
BEGIN
CREATE TABLE [dbo].[TiposMovimientos](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Nombre] [varchar](150) NOT NULL,
	[Habilitado] [bit] NOT NULL)
END;
GO

if NOT EXISTS (select 1 from sys.tables where NAME = 'Categorias')
BEGIN
CREATE TABLE [dbo].[Categorias](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Nombre] [varchar](150) NOT NULL,
	[Habilitada] [bit] NOT NULL,
	[Id_Tipo_Movimiento] [int] NOT NULL FOREIGN KEY REFERENCES TiposMovimientos (Id)
)
END;
GO
if NOT EXISTS (select 1 from sys.tables where NAME = 'Cuentas')
BEGIN
CREATE TABLE [dbo].[Cuentas](
	[Id] [int] IDENTITY(1,1) NOT NULL  PRIMARY KEY,
	[Nombre] [varchar](150) NOT NULL,
	[Es_Tarjeta_Credito] [bit] NOT NULL,
	[Dia_Cierre] [int] NULL,
	[Dia_Vencimiento] [int] NULL,
	[Habilitada] [bit] NOT NULL)
END;
GO
if NOT EXISTS (select 1 from sys.tables where NAME = 'Movimientos')
BEGIN
CREATE TABLE [dbo].[Movimientos](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Fecha_Carga] [datetime2](7) NOT NULL,
	[Monto] [float] NOT NULL,
	[Descripcion] [varchar](500) NULL,
	[Id_Cuenta] [int] NOT NULL FOREIGN KEY REFERENCES Cuentas (Id),
	[Id_Tipo_Movimiento] [int] NOT NULL FOREIGN KEY REFERENCES TiposMovimientos (Id),
	[Id_Categoria] [int] NOT NULL FOREIGN KEY REFERENCES Categorias (Id))
END;
GO

if NOT EXISTS (select 1 from TiposMovimientos where Nombre = 'Ingreso')
BEGIN
INSERT INTO [dbo].[TiposMovimientos] (Nombre, Habilitado) VALUES('Ingreso',1);
END;
go
if NOT EXISTS (select 1 from TiposMovimientos where Nombre = 'Egreso')
BEGIN
INSERT INTO [dbo].[TiposMovimientos] (Nombre, Habilitado) VALUES('Egreso',1);
END;

if NOT EXISTS (select 1 from TiposMovimientos where Nombre = 'Transaccion')
BEGIN
INSERT INTO [dbo].[TiposMovimientos] (Nombre, Habilitado) VALUES('Transaccion',1);
END;